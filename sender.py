import os
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver import DesiredCapabilities
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.chrome.options import Options

proxy = None #'35.171.2.189:8008'
number_of_times = 6
time_betweeen_messages = 1
name = '+55 12 98117-4661'
message = ['Avozilla','Rules','now','List','of', 'messages!']


## Selenium web drivers
driver = None

def wait():
    time.sleep(time_betweeen_messages)

def web_driver_load():
    print('Abrindo webdriver: Chrome')
    global driver
    global proxy

    options = Options()
    if proxy:
        print('Definindo Proxy: ' + proxy)
        options.add_argument('--proxy-server=%s' % proxy)

    binary_location = '{}/chromedriver'.format(os.path.abspath(os.path.dirname(__file__)))
    options.add_argument('--user-data-dir={}'.format(os.path.abspath(os.path.dirname(__file__))+"/tmp"))

    options.add_argument('headless')
    driver = webdriver.Chrome(executable_path=binary_location, chrome_options=options)


def web_driver_quit():
    driver.quit()
    quit()

def whatsapp_login():
    driver.get('https://web.whatsapp.com/');
    while True:
        time.sleep(3)
        try:
            appLoad = driver.find_element_by_xpath("//div[starts-with(@class, 'app')]")
            if appLoad:
                gotoChathead(name)
                break
        except NoSuchElementException:
            print('Aguardando Login')
            pass


def sendMessage(msg, index):
    web_obj = driver.find_element_by_xpath("//div[@contenteditable='true']")

    if type(msg) is list:
        web_obj.send_keys(msg[index])
    else:
        web_obj.send_keys(msg)

    web_obj.send_keys(Keys.RETURN)

def gotoChathead(name):
    # recentList = driver.find_elements_by_xpath("//span[@class='emojitext ellipsify']")    
    recentList = driver.find_elements_by_xpath("//span[@title='{}']".format(name))

    for head in recentList:
        if head.text == name:
            head.click()
            break


### Main Method
if __name__ == "__main__":
    print('Carregando webdriver')
    web_driver_load()
    print('Fazendo Login')
    whatsapp_login()
    print('Enviando mensagens')
    count=0
    for i in range(number_of_times):
        sendMessage(message, count)
        wait()
        count += 1
    print("Processo finalizado")
    web_driver_quit()