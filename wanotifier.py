import os
import sys
import time
import requests
import socketserver
from urllib import parse
from http.server import BaseHTTPRequestHandler,HTTPServer
from datetime import datetime
from threading import Thread
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver import DesiredCapabilities
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.options import Options

PORT=8000
IP=requests.get("https://api.ipify.org/?format=json").json()['ip']

class RequestHandler(BaseHTTPRequestHandler):
    
    def do_HEAD(self):
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()

    def do_GET(self):
        print('Webserver: Receiving GET')
        sendReply = False

        if os.path.isfile(os.curdir + self.path):
            if self.path.endswith(".png"):
                mimetype='image/png'
                print(self.path)
                file = open(os.curdir + self.path, "rb") 
                print(file)
                self.send_response(200)
                self.send_header('Content-type',mimetype)
                self.end_headers()
                self.wfile.write(file.read())
                file.close()
        
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()
        self.wfile.write("""
<html>
<script>setTimeout(document.getElementById("img-screenshot").src="screenshot.png",1000);</script>
<body>
    <form method="POST" action="#">
    to: (ex +5512981174661)</br><input type="text" name="to" value=""></br>
    message:</br><textarea name="message"></textarea></br>
    <input type="submit" value="enviar"></br>
    curl -X POST -d "to=+5512981174661&message=Mensage%20aqui" "{IP}:{PORT}" 
    <hr><img id="img-screenshot" src="screenshot.png"></body>
</html>
            """.format(IP=IP, PORT=PORT).encode('utf-8'))

        
    def do_POST(self):
        print('Webserver: Receiving POST')
        request_path = self.path
        request_headers = self.headers
        content_len = int(self.headers.get('content-length', 0))
        post_body = self.rfile.read(content_len)
        post_dict = parse.parse_qsl(parse.urlsplit(post_body).path)
        print("Writing file")

        directory = "messages"
        filename = "{dt:%Y%m%d%H%M%S}.txt".format(dt=datetime.now())

        file = open("{}/.{}".format(directory, filename), 'a')
        file.write(post_dict[0][1].decode('utf-8'))
        file.write("\n")
        file.write(post_dict[1][1].decode('utf-8')) 
        file.close() 
        
        os.rename("{}/.{}".format(directory, filename),"{}/{}".format(directory, filename))

        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()
        if post_dict[2][1].decode('utf-8') == "enviar":
            self.wfile.write("""
<META http-equiv="refresh" content="1;URL=#"> 
Mensagem recebida. Redirecionando
                """.encode('utf-8'))
    
    do_PUT = do_POST
    do_DELETE = do_GET
        

def startWebServer():
    print('Listening requests at port:%s' % PORT)
    server = HTTPServer(('', PORT), RequestHandler)
    server.serve_forever()



def sendMessage(driver, msg):
    print('\nEnviando Mensagem: {}\n'.format(msg))
    driver.get_screenshot_as_file('screenshot.png')
    WebDriverWait(driver, 40).until(
        EC.presence_of_element_located((By.XPATH, "//div[@contenteditable='true']")))
    web_obj = driver.find_element_by_xpath("//div[@contenteditable='true']")
    driver.get_screenshot_as_file('screenshot.png')
    if type(msg) is list:
        web_obj.send_keys(msg[index])
    else:
        web_obj.send_keys(msg)

    web_obj.send_keys(Keys.RETURN)
    time.sleep(4)



def openBrowser():

    options = Options()
    # options.headless = True
    options.add_argument('--user-data-dir=/home/eduardo/Dropbox/chrome2/')
    options.add_argument('--proxy-server=35.237.68.201:8008')
    options.add_argument("--no-sandbox")
    options.add_argument("--disable-dev-shm-usage")

    driver = webdriver.Chrome(options=options)
   
    return driver

def getScreenshot(driver):
    while True:
        time.sleep(2)
        if driver.get_screenshot_as_file('screenshot.png'):
            print("screenshot generated")



def main():
    if os.path.exists("screenshot.png"):
        os.remove("screenshot.png")
    
    Thread(target=startWebServer).start()
    
    driver = openBrowser()
    driver.get('https://web.whatsapp.com/');
    Thread(target=getScreenshot, args=[driver]).start()

    while True:
        if driver.find_elements_by_xpath("//span"):
            break
        else:
            print("*** Aguardando Autenticação WhatsApp")
            time.sleep(1)
    
    while True:

        path = "messages/"
        path_trash = "messages_trash/"

        for f in os.listdir(path):
            if f[:1] == "." or f[-4:] != ".txt":
                continue

            client=""
            message=""
            filename = path + "." + f
            os.rename(path + f,filename)
            print(filename)
            file = open(filename, 'r+')
            
            countline = 0
            for line in file:
                countline = countline+1
                if countline == 1:
                    client = line.replace('\n', '')
                if countline == 2:
                    message = line.replace('\n', '')
            
            # print(client,message)
            # print("//span[@title='{}']".format(client))
            try:
                driver.find_elements_by_xpath("//span[@title='{}']".format(client))[0].click()
            except:
                driver.get('https://web.WhatsApp.com/send?phone={}'.format(client.replace("+","")))

            time.sleep(1)
            sendMessage(driver, message)

            os.rename(filename,path_trash + f)

        print('.', end="")
        sys.stdout.flush()
        time.sleep(1)



try:
    main()

except Exception as e:
    driver.close()
    print("Erro - {}. Reabrindo o navegador ".format(e))
    main()



    
